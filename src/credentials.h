// WiFi Credentials
#define WIFI_SSID "YOUR-SSID"
#define WIFI_PASSWORD "YOUR-PASSWORD"

// Telegram Bot Token (get it from Botfather)
#define BOTtoken "YOUR-TELEGRAM-BOT-TOKEN" 

// Unique ID of the Shock Collar. You can also keep this and use pairing mode on the collar
String uniqueKeyOfDevice = "0010110011011000";

// Credentials for EasyDDNS Service
#define ddnsService "duckdns"
#define ddnsDomain "Enter your ddns domain here"
#define ddnsToken "Enter your ddns token here"

// MQTT Credentials
const char* mqtt_server = "192.168.0.1";  
uint16_t mqtt_port = 1883;
#define mqtt_clientid "shocker"
#define mqtt_username "your_MQTT_Username"
#define mqtt_password "Your_MQTT_Password"
#define mqtt_publishtopic "shocker/inTopic"
#define mqtt_subscribetopic "shocker/outTopic"