// NOTES
// Kinky Telegram Bot that controls a 433mhz shock collar
// Tested on a Wemos D1 Mini and NodeMCU with the 433mhz transmitter conencted to pin D6 and a button connected to D2

/// IMPORTANT /// THE WIFI MANAGER IS CURRENTLY NOT INCLUDED SO THE BUTTON HAS NO FUNCTION
// To trigger the Wifi Manager AP Mode keep the button pressed while pluggin the microcontroller in 
// Button gets only checked during setup routine to prevent accidental resets

// activate / deactivate MQTT Client
#define MQTT 1

// activate deactivate serial output for debugging
#define DEBUG 0
#if DEBUG == 1
#define debug(x) Serial.print(x)
#define debugln(x) Serial.println(x)
#else
#define debug(x)
#define debugln(x)
#endif


#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
#include "DogCollar3.h"
#include <EasyDDNS.h>
#include <credentials.h>

#define TRIGGER_PIN 4 // pin d2 on wemos
#define PIN_TRANSMITTER 12  //which is d6 on the d1mini
DogCollar dg(PIN_TRANSMITTER,uniqueKeyOfDevice);

X509List cert(TELEGRAM_CERTIFICATE_ROOT);
WiFiClientSecure secured_client; // for Telegram
UniversalTelegramBot bot(BOTtoken, secured_client);

const long Bot_mtbs = 3000; //mean time between scan messages
unsigned long Bot_lasttime;   //last time messages' scan has been done
bool Start = false;
int vibration;
int shock;
int beep;
int keepawake;
unsigned long keepawakeinterval = 120000;  // 120000 = 2 minutes
unsigned long previousLedWakeUp = 0;
bool firststart = true;
bool firstcleanupmessage = true;
bool cleanUpDone = false;
bool cleanUpMessageDone = false;


//pubsub start
#if MQTT == 1
#include <PubSubClient.h>
WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE	(50)
char msg[MSG_BUFFER_SIZE];
long int value = 0;
const long mqtt_reconnect_delay = 5000; // MQTT Reconnection interval if disconnected
unsigned long MQTTpreviousMillis = 0;


void callback(char* topic, byte* payload, unsigned int length) {
  debug("Message arrived [");
  debug(topic);
  debug("] ");

  char messageBuffer[30];  //somewhere to put the message
  memcpy(messageBuffer, payload, length);  //copy in the payload
  messageBuffer[length] = '\0';  //convert copied payload to a C style string
  String receivedMQTTmessage = messageBuffer;
  debugln(messageBuffer);  //print the buffer if you want to

  if (strcmp(messageBuffer, "beep") == 0)
    {
    //do something if there is a match
    dg.sendCollar(CollarChannel::CH1, CollarMode::Beep, 100);
    debugln("beep triggered by MQTT");
    client.publish(mqtt_publishtopic, messageBuffer);
    }

  if (strncmp(messageBuffer, "vib", 3) == 0)
    {
      debugln("vib via MQTT detected");
      if (receivedMQTTmessage.startsWith("vib")){
      vibration = receivedMQTTmessage.substring(3).toInt();
      String sendvibpremessage = "Collar vibrated at Level ";
      String sendvibmessage = sendvibpremessage + vibration;
      //do something if there is a match
      dg.sendCollar(CollarChannel::CH1, CollarMode::Vibe, vibration);
      debugln(sendvibmessage);
      client.publish(mqtt_publishtopic, messageBuffer);
      }
    }

  if (strncmp(messageBuffer, "shock", 5) == 0)
    {
      debugln("shock via MQTT detected");
      if (receivedMQTTmessage.startsWith("shock")){
      shock = receivedMQTTmessage.substring(5).toInt();
      String sendshockpremessage = "Shocked at Level ";
      String sendshockmessage = sendshockpremessage + shock;
      //do something if there is a match
      dg.sendCollar(CollarChannel::CH1, CollarMode::Shock, shock);
      debugln(sendshockmessage);
      client.publish(mqtt_publishtopic, messageBuffer);
      }
    }

  if (strcmp(messageBuffer, "keepawakeon") == 0)
    {
    //do something if there is a match
    keepawake = 1;
    debugln("keep awake enabled");
    client.publish(mqtt_publishtopic, messageBuffer);
    }
  
  if (strcmp(messageBuffer, "keepawakeoff") == 0)
    {
    //do something if there is a match
    keepawake = 0;
    debugln("keep awake disabled");
    client.publish(mqtt_publishtopic, messageBuffer);
    }
  
} 


void reconnect() {
  unsigned long currentMillis = millis();  
  // Loop until we're reconnected
  while (!client.connected()) {
    debug("Attempting MQTT connection...");
    // Attempt to connect
      if (currentMillis - MQTTpreviousMillis >= mqtt_reconnect_delay) {
        MQTTpreviousMillis = currentMillis;
        if (client.connect(mqtt_clientid, mqtt_username, mqtt_password)) {
          debugln("connected");
          // Once connected, publish an announcement...
          client.publish(mqtt_publishtopic, "Shock1Connected");
          // ... and resubscribe
          client.subscribe(mqtt_subscribetopic);
        } else {
          debug("failed, rc=");
          debug(client.state());
          debugln(" try again in 5 seconds");
          // Wait 5 seconds before retrying
        }
      }
  }
}
#endif
// pubsub end


// Start handleNewMessages
void handleNewMessages(int numNewMessages) {
  debugln("handleNewMessages");
  debugln(String(numNewMessages));

  for (int i=0; i<numNewMessages; i++) {
    String chat_id = String(bot.messages[i].chat_id);
    String text = bot.messages[i].text;
   
    debugln(text);
   
    String from_name = bot.messages[i].from_name;
    if (from_name == "") from_name = "Guest";


  if (firststart == false && cleanUpMessageDone == true) {
    if (text.startsWith("/vib")){
      vibration = text.substring(4).toInt();
      String sendvibpremessage = "Collar vibrated at Level ";
      String sendvibmessage = sendvibpremessage + vibration;
      dg.sendCollar(CollarChannel::CH1, CollarMode::Vibe, vibration);
      bot.sendMessage(chat_id, sendvibmessage, "Markdown");
    }


    if (text.startsWith("/shock")){
      shock = text.substring(6).toInt();
      String sendshockpremessage = "Collar shocked at Level ";
      String sendshockmessage = sendshockpremessage + shock;
      dg.sendCollar(CollarChannel::CH1, CollarMode::Shock, shock);
      bot.sendMessage(chat_id, sendshockmessage, "Markdown");
    }



    if (text == "/beep") {
      dg.sendCollar(CollarChannel::CH1, CollarMode::Beep, 100);
      bot.sendMessage(chat_id, "Collar Beeped", "");
    }


    if (text == "/help") {
      String welcome = "Welcome to the kinky shock bot from, " + from_name + ".\n";
      welcome += "/beep : to trigger a beep\n";
      welcome += "/vib1 : to trigger a vibration where 1 can be a value from 1 to 100\n";
      welcome += "/shock1 : to trigger a shock where 1 can be a value from 1 to 100\n";
      welcome += "/menu : to get quick access to some settings\n";
      welcome += "/keepawakeon : blinks the collar led every 2 min to prevent sleep\n";
      welcome += "/keepawakeoff : turns the keep awake function off\n";
      welcome += "/keepawakestatus : checks if the KeepAwake Feature is turned on or off\n";
      bot.sendMessage(chat_id, welcome, "Markdown");
    }

        
      if (text == "/start") {
        String welcome = "Welcome to the kinky shock bot from, " + from_name + ".\n";
        welcome += "/beep : to trigger a beep\n";
        welcome += "/vib1 : to trigger a vibration where 1 can be a value from 1 to 100\n";
        welcome += "/shock1 : to trigger a shock where 1 can be a value from 1 to 100\n";
        welcome += "/menu : to get quick access to some settings\n";
        welcome += "/keepawakeon : blinks the collar led every 2 min to prevent sleep\n";
        welcome += "/keepawakeoff : turns the keep awake function off\n";
        welcome += "/keepawakestatus : checks if the KeepAwake Feature is turned on or off\n";
        bot.sendMessage(chat_id, welcome, "Markdown");
      }
    
    if (text == "/menu") {
          String keyboardJson = "[[\"/vib25\", \"/vib50\", \"/vib75\", \"/vib100\"],[\"/shock25\", \"/shock50\", \"/shock75\", \"/shock100\"]]";
          bot.sendMessageWithReplyKeyboard(chat_id, "Choose from one of the following options", "", keyboardJson, true);
        }

    if (text == "/keepawakeon") {
      keepawake = 1;
      bot.sendMessage(chat_id, "Keep Awake enabled", "");
    }

    if (text == "/keepawakeoff") {
      keepawake = 0;
      bot.sendMessage(chat_id, "Keep Awake disabled", "");
    }

    if (text == "/keepawakestatus") {
      String awakestatus;
      String awakestatusmessage = "The keep awake function is ";
      if (keepawake == 0) {
        awakestatus = "disabled";
      }
        else if (keepawake == 1) {
          awakestatus = "enabled";
        }
      String sendawakestatusmessage = awakestatusmessage + awakestatus;
      bot.sendMessage(chat_id, sendawakestatusmessage, "");
      }
    }

    if (firstcleanupmessage == true) {
      bot.sendMessage(chat_id, "Shocker started & connected. Cleaning up missed commands", "");
      firstcleanupmessage = false;
    }

    if (cleanUpDone == true && cleanUpMessageDone == false) {
      bot.sendMessage(chat_id, "Cleanup Process done. Ready to have some fun", "");
      cleanUpMessageDone = true;
      cleanUpDone = false;
    }
  }

}
// END handleNewMessages

void setup()
  {
    Serial.begin(115200);
    pinMode(TRIGGER_PIN, INPUT_PULLUP);
      
    // attempt to connect to Wifi network:
    debug("Connecting to Wifi SSID ");
    debug(WIFI_SSID);
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    secured_client.setTrustAnchors(&cert); // Add root certificate for api.telegram.org
    while (WiFi.status() != WL_CONNECTED)
    {
      debug(".");
      delay(500);
    }
    debug("\nWiFi connected. IP address: ");
    debugln(WiFi.localIP());

    debug("Retrieving time: ");
    configTime(0, 0, "pool.ntp.org"); // get UTC time via NTP
    time_t now = time(nullptr);
    while (now < 24 * 3600)
    {
      debug(".");
      delay(100);
      now = time(nullptr);
    }
    debugln(now);

  
    // Collar setup and link routine
    debugln("collar setup routine starting");
    // trying different modes of Channel 1-coded device in a loop...
    debugln("CollarChannel::CH1, CollarMode::Beep, Strength:50");
    delay(1000);
    dg.sendCollar(CollarChannel::CH1, CollarMode::Beep, 50);

    delay(1000);

    debugln("CollarChannel::CH1, CollarMode::Vibe, Strength:50");
    delay(1000);
    dg.sendCollar(CollarChannel::CH1, CollarMode::Vibe, 50);

    delay(1000);

    debugln("CollarChannel::CH1, CollarMode::Beep, Strength:50");
    delay(1000);
    dg.sendCollar(CollarChannel::CH1, CollarMode::Beep, 50);

    delay(1000);

    debugln("CollarChannel::CH1, CollarMode::Vibe, Strength:50");
    delay(1000);
    dg.sendCollar(CollarChannel::CH1, CollarMode::Vibe, 50);
    delay(1000);

    //reduce traffic eventually - needs to be further tested
    // bot.longPoll = 50;

    // for EasyDDNS
    EasyDDNS.service(ddnsService);
    EasyDDNS.client(ddnsDomain,ddnsToken);    // Enter ddns Domain & Token | Example - "esp.duckdns.org","1234567"
    EasyDDNS.onUpdate([&](const char* oldIP, const char* newIP){
    debug("EasyDDNS - IP Change Detected: ");
    debugln(newIP);
  });
  debugln("Setup done");

  //pubsub
  #if MQTT == 1
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  #endif
  //pubsub end
}




void loop() {
  EasyDDNS.update(30000); // check for new IP every 30 seconds

  //pubsub start
  #if MQTT == 1
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  if (firststart == true) {
    client.publish(mqtt_publishtopic, "Shock1Started");
  }
  #endif
  //pubsub end

  if (firststart == true) {
    debugln("first start");
    int numNewMessages = bot.getUpdates(bot.last_message_received + 1);

    while(numNewMessages) {
      handleNewMessages(numNewMessages);
      numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    }

    if (numNewMessages == 0) {
      // bot.sendMessage(chat_id, "Cleanup Process done. Ready to have some fun", "");
      debugln("new messages = 0");
      cleanUpDone = true;
      firststart = false;
      // bot.longPoll = 50;
      handleNewMessages(1);
      }


  }

  if (millis() > Bot_lasttime + Bot_mtbs && firststart == false)  {
    int numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    debugln("getting telegram updates");

    while(numNewMessages) {
      debugln("got response");
      handleNewMessages(numNewMessages);
      numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    }

    Bot_lasttime = millis();
  }

  unsigned long keepAwakeMillis = millis();
  if (keepAwakeMillis - previousLedWakeUp >= keepawakeinterval && keepawake == 1) {
    previousLedWakeUp = keepAwakeMillis;
    dg.sendCollar(CollarChannel::CH1, CollarMode::Blink, 100);
  }
}


